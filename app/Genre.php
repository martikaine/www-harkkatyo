<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    //
    public function posts()
    {
        return $this->belongsToMany('App\Post');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
