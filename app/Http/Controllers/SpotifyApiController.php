<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class SpotifyApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function getCurrentlyPlaying()
    {
        return $this->makeApiRequest('/v1/me/player/currently-playing');
    }

    public function getAlbumInfo(string $albumId)
    {
        $response = $this->makeApiRequest( '/v1/albums/' . $albumId);
        return $response;
    }

    public function getArtistInfo(string $artistId)
    {
        $response = $this->makeApiRequest( '/v1/artists/' . $artistId);
        return $response;
    }

    /**
     * Makes a request to the Spotify API.
     *
     * @param $apiUrl   string  endpoint to call (eg. '/v1/me/player/currently-playing')
     * @return          string  JSON format response from the API
     */
    private function makeApiRequest(string $apiUrl)
    {
        $token = session('spotify_token');

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.spotify.com" . $apiUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $token
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        if($err) {
            Log::error($err);
        } else {
            Log::info('querying: ' . "https://api.spotify.com" . $apiUrl);
            Log::info($token .' '. $response);
        }
        return $response;
    }
}