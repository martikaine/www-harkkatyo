<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Genre;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Create a new post.
     * Saves the Spotify URI, post text and all associated genres to the database.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function post(Request $request)
    {
        $post = new Post;
        $post->spotify_uri = $request->input('spotifyUri');
        $post->text = $request->input('text');
        $post->user_id = Auth::id();

        $post->save();

        $genres = json_decode($request->input('genres'), true);
        if($genres) {
            foreach ($genres as $genreName) {
                $genre = Genre::firstOrCreate(['name' => $genreName]);
                $genre->posts()->attach($post->id);
            }
        }

        Cache::forever('newestPostDate', $post->created_at);

        return redirect('/home');
    }

    /**
     * Checks if there are new posts.
     *
     * @return \Illuminate\Http\JsonResponse {hasNewPosts: (true|false)}
     */
    public function hasNewPosts()
    {
        $hasNewPosts = (!Cache::has('newestPostDate') || !Session::has('newestPostInFeed') ||
                        strtotime(Cache::get('newestPostDate')) < strtotime(Session::get('newestPostInFeed')));

        return response()->json(['hasNewPosts' => $hasNewPosts], 200);
    }

    /**
     * Loads the posts partial view.
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function getPosts()
    {
        $posts = Post::all()->sortByDesc('created_at');
        if ($posts->count() > 0)
        {
            Session::put('newestPostInFeed', $posts->first()->created_at);
            Cache::forever('newestPostDate', $posts->first()->created_at);
        }

        return view('posts', ['posts' => $posts]);
    }

    /**
     * Deletes a post if the user is allowed to do so.
     * @param int $id   id of post to be deleted
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deletePost(int $id)
    {
        $post = Post::findOrFail($id);

        if(Auth::check() && (Auth::user()->is_admin || Auth::id() == $post->user->id))
        {
            $post->delete();
        }

        return redirect('/home');
    }
}
