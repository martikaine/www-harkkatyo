<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login()
    {
        return Socialite::with('spotify')
            ->scopes(['user-read-currently-playing'])
            ->redirect();
    }

    /**
     * Spotify auth flow redirects here.
     * Registers the user if they are new and logs them in.
     * Saves Spotify API access credentials in the session.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function callback(Request $request)
    {
        $user = Socialite::driver('spotify')->stateless()->user();
        Session::put(['spotify_token' => $user->token]);
        Session::put(['spotify_user_id' => $user->id]);

        try {
            $authUser = User::where('spotify_user_id', $user->id)->firstOrFail();
            Log::info('Logging in as ' . $authUser->name);
            Auth::login($authUser, true);
            return redirect($this->redirectTo);

        } catch (ModelNotFoundException $e) {
            Log::info('redir to /register');
            return redirect('/register');
        }
    }
}
