<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <style>
        .full-page {
            height: 100vh;
        }
    </style>
</head>
<body>
    <div class="full-page">
        <div class="d-flex align-items-center flex-column justify-content-center h-100">
            <h1 class="display-4">Discover and share music like never before.</h1>
            <div class="mt-4">
                <a href="{{ route('login') }}" class="btn btn-success btn-lg" id="btn-connect">Login with Spotify</a>
            </div>
            <div class="mt-2">
                <a href="{{ route('home') }}">View as guest</a>
            </div>
        </div>
    </div>
</body>
</html>
