@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Post</div>
                <div class="card-body">
                    @auth
                    <form action="{{ route('post') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="spotifyUri" id="spotifyUri">
                        <input type="hidden" name="genres">

                        <div class="row">
                            <div class="col-10">
                                <input type="text" class="form-control" id="search" placeholder="Search for a song..." disabled>
                            </div>
                        </div>
                        <div class="card-text">
                            <small class="text-muted" id="genres-display"></small>
                        </div>
                        <div class="row mt-3">
                            <div class="col-10">
                                <textarea class="form-control" name="text" placeholder="What do you think about this song?"></textarea>
                            </div>
                            <div class="col">
                                <button type="submit" style="height:100%" class="btn btn-primary btn-lg btn-block">Post</button>
                            </div>
                        </div>
                    </form>
                    @else
                    <div class="row mt-3 justify-content-center">
                        <a href="{{ route('login') }}" class="btn btn-success btn-lg" id="btn-connect">Login with Spotify</a>
                    </div>
                    @endauth
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row mt-5">
        <div class="col-md-12" id="posts">
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ mix('js/feedAjax.js') }}"></script>
@endsection