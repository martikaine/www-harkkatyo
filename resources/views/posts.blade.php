@foreach($posts as $post)
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    {{ $post->user->name }}<br>
                    <img src="/img/profile_dummy.png" width="120" height="120">
                </div>
                <div class="col">
                    <p class="card-text">
                        <small class="text-muted">{{ $post->created_at }}</small>
                        <span class="justify-content-end">
                            <small class="text-muted">
                                @foreach($post->genres as $genre)
                                    #{{ $genre->name }}
                                @endforeach
                            </small>
                        </span>
                    </p>

                    <p class="card-text" style="white-space:pre-line;">{{ $post->text }}</p>
                    <div class="embed-responsive embed-responsive-1by1" style="height:80px;">
                        <iframe src="https://open.spotify.com/embed?uri={{ $post->spotify_uri }}"
                                allowtransparency="true">
                        </iframe>
                    </div>
                    <br>
                    <a href="#">{{ $post->comments->count() }} comments</a>
                    @if(Auth::check() && (Auth::user()->is_admin || Auth::id() == $post->user->id))
                    <span class="justify-content-end">
                        <a href={{route('deletepost', ['id' => $post->id])}}>Delete</a>
                    </span>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endforeach