"use strict";

// Loads the feed partial view.
function loadPosts() {
    fetch('/posts', {credentials: 'include'})
        .then(response => response.text())
        .then(viewHTML => {
            if (viewHTML.length > 0) {
                $("#posts").html(viewHTML)
            }
        });
}

// Polls the server for new posts and loads them if they exist
function pollForNewPosts() {
    fetch('/posts/poll', {credentials: 'include'})
        .then(response => response.json())
        .then(json => {
            if(json.hasNewPosts) {
                loadPosts()
            }
        });
}

// Gets current track name, artist and genres for the post form.
function getCurrentlyPlaying() {
    fetch('/api/nowplaying', {credentials: "include"})
        .then(response => response.json())
        .then(nowPlaying => {
            let artist = nowPlaying.item.artists[0].name;
            let song = nowPlaying.item.name;

            $('input[name=spotifyUri]').val(nowPlaying.item.uri);
            $('#search').val(artist + ' - ' + song);

            return fetch('/api/artist/' + nowPlaying.item.artists[0].id, {credentials: 'include'});
        })
        .then(response => response.json())
        .then(artist => {
            let genres = artist.genres.map(genre => genre.replace(/\s+/g, '')); // remove spaces
            $('input[name=genres]').val(JSON.stringify(genres));
            genres.forEach(genre => $('#genres-display').append('#' + genre + ' '));
        });
}



$(document).ready(function() {
    loadPosts();
    getCurrentlyPlaying();

    let TEN_SECONDS = 10000;
    setInterval(pollForNewPosts, TEN_SECONDS);
    setInterval(getCurrentlyPlaying, TEN_SECONDS);
});
