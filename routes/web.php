<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

Route::get('/login', 'Auth\LoginController@login')->name('login');
Route::get('/spotifycallback', 'Auth\LoginController@callback')->name('spotifycallback');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register')->name('register');

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/post', 'HomeController@post')->name('post');
Route::get('/posts', 'HomeController@getPosts')->name('posts');
Route::get('/posts/poll', 'HomeController@hasNewPosts');
Route::get('/posts/delete/{id}', 'HomeController@deletePost')->name('deletepost');

Route::get('/api/nowplaying', 'SpotifyApiController@getCurrentlyPlaying');
Route::get('/api/album/{albumId}', 'SpotifyApiController@getAlbumInfo');
Route::get('/api/artist/{artistId}', 'SpotifyApiController@getArtistInfo');