/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 44);
/******/ })
/************************************************************************/
/******/ ({

/***/ 44:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(45);


/***/ }),

/***/ 45:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// Loads the feed partial view.

function loadPosts() {
    fetch('/posts', { credentials: 'include' }).then(function (response) {
        return response.text();
    }).then(function (viewHTML) {
        if (viewHTML.length > 0) {
            $("#posts").html(viewHTML);
        }
    });
}

// Polls the server for new posts and loads them if they exist
function pollForNewPosts() {
    fetch('/posts/poll', { credentials: 'include' }).then(function (response) {
        return response.json();
    }).then(function (json) {
        if (json.hasNewPosts) {
            loadPosts();
        }
    });
}

// Gets current track name, artist and genres for the post form.
function getCurrentlyPlaying() {
    fetch('/api/nowplaying', { credentials: "include" }).then(function (response) {
        return response.json();
    }).then(function (nowPlaying) {
        var artist = nowPlaying.item.artists[0].name;
        var song = nowPlaying.item.name;

        $('input[name=spotifyUri]').val(nowPlaying.item.uri);
        $('#search').val(artist + ' - ' + song);

        return fetch('/api/artist/' + nowPlaying.item.artists[0].id, { credentials: 'include' });
    }).then(function (response) {
        return response.json();
    }).then(function (artist) {
        var genres = artist.genres.map(function (genre) {
            return genre.replace(/\s+/g, '');
        }); // remove spaces
        $('input[name=genres]').val(JSON.stringify(genres));
        genres.forEach(function (genre) {
            return $('#genres-display').append('#' + genre + ' ');
        });
    });
}

$(document).ready(function () {
    loadPosts();
    getCurrentlyPlaying();

    var TEN_SECONDS = 10000;
    setInterval(pollForNewPosts, TEN_SECONDS);
    setInterval(getCurrentlyPlaying, TEN_SECONDS);
});

/***/ })

/******/ });